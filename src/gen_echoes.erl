%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A multi-instance echo server (supervisable).
%%%      As improvement over the `gen_echo' implementation, this
%%%      version allows to start more than one echo server, by using
%%%      auto-generated atoms for process registration.
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echoes).
-behaviour(gen_server).

%% PUBLIC API
-export([echo/2]).

%% SUPERVISOR API
-export([start/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

%% @doc Starts an echo server, to be registered as `Name'.
-spec start(Name :: atom()) -> {ok, Pid :: pid()} |
                               {error, Error :: {already_started, pid()}} |
                               {error, Error :: term()} |
                               ignore.
start(Name) ->
    gen_server:start_link({local, Name}, ?MODULE, [Name], []).

%% @doc Sends a message to the `Name' echo server and returns its reply
-spec echo(Name :: atom(), Message :: any()) -> any().
echo(Name, Message) ->
    gen_server:call(Name, {echo, Message}).

% Internal
% @private
% @doc Initializes the server
-spec init(Args :: term()) -> {ok, State :: term()} |
                              {ok, State :: term(), Timeout :: timeout()} |
                              {ok, State :: term(), hibernate} |
                              {stop, Reason :: term()} |
                              ignore.
init([Name]) ->
    % process_flag(trap_exit, true), % we do want the process to die
    {ok, Name}.

% @private
% @doc Handling call messages
-spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
                         {reply, Reply :: term(), NewState :: term()} |
                         {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
                         {reply, Reply :: term(), NewState :: term(), hibernate} |
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
                         {stop, Reason :: term(), NewState :: term()}.
handle_call({echo, Message}, _From, Name) ->
    Reply = Message ++ " from " ++ atom_to_list(Name),
    {reply, Reply, Name}.

% @private
% @doc Handling cast messages
-spec handle_cast(Request :: term(), State :: term()) ->
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: term(), NewState :: term()}.
handle_cast(_Request, State) ->
    {noreply, State}.

% @private
% @doc Handling all non call/cast messages
-spec handle_info(Info :: timeout() | term(), State :: term()) ->
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(_Info, State) ->
    {noreply, State}.

% @private
% @doc
% This function is called by a gen_server when it is about to
% terminate. It should be the opposite of Module:init/1 and do any
% necessary cleaning up. When it returns, the gen_server terminates
% with Reason. The return value is ignored.
% @end
-spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
                State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

% @private
% @doc Convert process state when code is changed
-spec code_change(OldVsn :: term() | {down, term()},
                  State :: term(),
                  Extra :: term()) -> {ok, NewState :: term()} |
                                      {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

% @private
% @doc
% This function is called for changing the form and appearance
% of gen_server status when it is returned from sys:get_status/1,2
% or when it appears in termination error logs.
% @end
-spec format_status(Opt :: normal | terminate,
                    Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.
