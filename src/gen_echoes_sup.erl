%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A multi-instance echo server supervisor.
%%%      Supervises the echo server instances defined in the
%%%      `gen_echoes' module.
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echoes_sup).
-behaviour(supervisor).

%% API
-export([start/1]).

%% Supervisor callbacks
-export([init/1]).

-define(ECHO_SUPERVISOR, echo_sup).

%% @doc Starts the supervisor, which in turn starts `N' echo servers.
-spec start(N :: integer()) -> {ok, Pid :: pid()} |
                               {error, {already_started, Pid :: pid()}} |
                               {error, {shutdown, term()}} |
                               {error, term()} |
                               ignore.
start(N) ->
    supervisor:start_link({local, ?ECHO_SUPERVISOR}, ?MODULE, [N]).

% @private
% @doc
% Whenever a supervisor is started using supervisor:start_link/[2,3],
% this function is called by the new process to find out about
% restart strategy, maximum restart intensity, and child
% specifications.
% @end
-spec init(Args :: term()) ->
                  {ok, {SupFlags :: supervisor:sup_flags(),
                        [ChildSpec :: supervisor:child_spec()]}} |
                  ignore.
init([N]) ->

    SupFlags = #{strategy => one_for_one,
                 intensity => 1000, % times
                 period => 1},      % seconds

    Children = lists:map(fun(I) ->
                                 Id = list_to_atom("echo" ++ integer_to_list(I)),
                                 #{id => Id,
                                   start => {gen_echoes, start, [Id]},
                                   restart => transient,
                                   shutdown => 500,
                                   type => worker,
                                   modules => [gen_echoes]}
                         end, lists:seq(1, N)),

    {ok, {SupFlags, Children}}.
