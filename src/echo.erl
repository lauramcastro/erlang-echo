%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc An echo server
%%% @end
%%%-------------------------------------------------------------------
-module(echo).

%% PUBLIC API
-export([start/0, stop/0, echo/1]).

%% PRIVATE API (we need to export loop/0 in order to be able to spawn)
-export([loop/0]).

-define(ECHO_SERVER, echo).

%% @doc Starts the echo server
-spec start() -> ok.
start() ->
    true = register(?ECHO_SERVER, spawn(?MODULE, loop, [])),
    ok.

%% @doc Stops the echo server
-spec stop() -> ok.
stop() ->
    ?ECHO_SERVER ! stop,
    catch unregister(?ECHO_SERVER),
    ok.

%% @doc Sends a message `Message' to the echo process and returns its reply.
-spec echo(Message :: any()) -> any().
echo(Message) ->
    ?ECHO_SERVER ! {echo, Message, self()},
    receive
        {echo_reply, Reply} -> Reply
    end.

% Internal
% @private
loop() ->
    receive
        {echo, Message, Who} ->
            Who ! {echo_reply, Message},
            echo:loop(); % allows hot code updates
        stop ->
            ok;
        _Other ->
            % flush unknown messages
            echo:loop()
    after
        2000 ->
            ok % just shutdown if no one is using the server
    end.
