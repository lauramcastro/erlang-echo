%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc An echo server supervisor.
%%%      Supervises the echo server defined in the `gen_echo' module.
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echo_sup).
-behaviour(supervisor).

%% API
-export([start/0]).

%% Supervisor callbacks
-export([init/1]).

-define(ECHO_SUPERVISOR, echo_sup).

%% @doc Starts the supervisor, which in turn starts the echo server.
-spec start() -> {ok, Pid :: pid()} |
                 {error, {already_started, Pid :: pid()}} |
                 {error, {shutdown, term()}} |
                 {error, term()} |
                 ignore.
start() ->
    supervisor:start_link({local, ?ECHO_SUPERVISOR}, ?MODULE, []).

% @private
% @doc
% Whenever a supervisor is started using supervisor:start_link/[2,3],
% this function is called by the new process to find out about
% restart strategy, maximum restart intensity, and child
% specifications.
% @end
-spec init(Args :: term()) ->
                  {ok, {SupFlags :: supervisor:sup_flags(),
                        [ChildSpec :: supervisor:child_spec()]}} |
                  ignore.
init([]) ->

    SupFlags = #{strategy => one_for_one,
                 intensity => 1000, % times
                 period => 1},      % seconds

    AChild = #{id => echo,
               start => {gen_echo, start, []},
               restart => transient,
               shutdown => 500,
               type => worker,
               modules => [gen_echo]},

    {ok, {SupFlags, [AChild]}}.
