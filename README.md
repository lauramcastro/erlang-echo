# Erlang Echo

This is a demonstrator for Erlang capabilities when implementing fault-tolerant architectures... and testing them!

##  Description

This demonstrator is built as a four-step exercise from a simple echo server to a version with uses Erlang behaviours and supports distribution. Diagrams below follow the [C4 representation model](https://c4model.com) for software architecture.

### echo server

Module `echo` defines a simple echo server implemented with a custom process. The project includes a few traditional unit tests (module `echo_tests`) and a PBT stateful model `echo_props`).

<img src="./doc/Echo.png" alt="simple echo diagram" height="300"/>

### gen_server-based echo server (with supervision)

Module `gen_echo` redefines the echo server, now using the [`gen_server`](http://erlang.org/doc/design_principles/gen_server_concepts.html) OTP behaviour. Taking advantage of this, a [supervisor](http://erlang.org/doc/design_principles/sup_princ.html) is included (`gen_echo_sup`). A PBT stateful model `gen_echo_props`) is provided.

<img src="./doc/SupervisedEcho.png" alt="gen_server-based echo diagram" height="450"/>

### multiple-instance echo server

Module `gen_echoes` modifies the previous gen_server-based version sligthly to achieve the capability of having multiple instances of echo servers, under the same supervisor (`gen_echoes_sup`). The corresponding PBT stateful model `gen_echoes_props`) is provided.

<img src="./doc/SupervisedEchoes.png" alt="multiple-instance echo diagram" height="600"/>

### distributed echo server

Last, module `dis_echoes` modifies in turn the previous multiple-instance gen_server-based version to incorporate distribution capabilities. The corresponding PBT stateful model `dis_echoes_props`) is provided.

<img src="./doc/DistributedEchoes.png" alt="distributed echo diagram" height="600"/>

Following the instructions below, tests on all versions but the distributed one can be successfully run, as shown on [this project GitLab's pipelines](https://gitlab.com/lauramcastro/erlang-echo/pipelines). At the moment, the test model of the distributed version hits an alleged bug in [PropER](https://proper-testing.github.io) that is yet to be reported. If you have a [QuickCheck](http://www.quviq.com/downloads/) licence, though, you can run the distributed tests as explained below.

## Dependencies

In order to build this project, you need [rebar3](https://www.rebar3.org).

## Build

    $ rebar3 compile
    
## Run tests

### EUnit

There are some anecdotic "traditional" [unit tests](http://erlang.org/doc/apps/eunit/chapter.html) that can be run with

    $ rebar3 eunit

### PropER

The ["proper"](https://proper-testing.github.io) tests for this project can be run with

    $ rebar3 proper

### QuickCheck

The tests for the distributed version can only be run using [QuickCheck](http://www.quviq.com/products/erlang-quickcheck/) at the moment. You can proceed as follows:

    $ rebar3 as testeqc do shell
    (testnode@yourhost.yourdomain)1> eqc:quickcheck(dis_echoes_props:prop_echoserver()).
    
Make sure you set up the macros in the `dis_echoes.hrl` before proceeding.

### Coverage

Coverage can be explored with

    $ rebar3 cover

Reported coverage may seem rather low, but bear in mind that the distributed code is not tested in GitLab's CI at the moment. An alternative to fix this would be to combine the QuickCheck models with a CT suite (TODO).

## Code analysis

Static analysis of the source code can be performed by running several tools.

* [Dialyzer](http://erlang.org/doc/man/dialyzer.html) is a static analysis tool that identifies type errors, amongst others. It is already part of your standard Erlang/OTP installation.

      $ rebar3 as test do dialyzer

  Bear in mind this process is very CPU-intensive and can take a while the first time it is launched (and every first time after a `rebar3 clean`).

* [Xref](http://erlang.org/doc/man/xref.html) is a cross reference tool.  It is already part of your standard Erlang/OTP installation.

      $ rebar3 xref

* [Elvis](https://github.com/inaka/elvis) is a style checker. You need to download and escriptize the tool (just follow the instructions), and then run:

      $ elvis rock



