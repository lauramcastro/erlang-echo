%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A multiple-instance supervisable echo server
%%%      (traditional test module)
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echoes_tests).

-include_lib("eunit/include/eunit.hrl").

-define(ECHO_SERVER, echo).

gen_echoes_terminate_normal_test() ->
    process_flag(trap_exit, true),
    {ok, Echo} = gen_echoes:start(?ECHO_SERVER),
    true = exit(Echo, shutdown),
    timer:sleep(10),
    ?assertNot(is_process_alive(Echo)).

gen_echoes_test_() ->
    {setup,
     fun() -> {ok, _Echo} = gen_echoes:start(?ECHO_SERVER) end,
     fun(_) ->  ok = gen_server:stop(?ECHO_SERVER) end,
     fun(_) ->
             Message = "Lambda World Cádiz",
             {inparallel,
              [?_assertEqual(ok, gen_server:cast(?ECHO_SERVER, Message)),
               ?_assertEqual(Message, ?ECHO_SERVER ! Message),
               ?_assertEqual({ok, state}, gen_echoes:code_change(oldvsn, state, extra)),
               ?_assertEqual([status], gen_echoes:format_status(normal, [status]))
              ]}
     end}.
