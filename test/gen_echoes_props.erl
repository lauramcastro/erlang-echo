%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A multi-instance supervised echo server
%%%      (test module)
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echoes_props).

-include_lib("proper/include/proper.hrl").

%% PropER callbacks
-export([initial_state/0, command/1, precondition/2, next_state/3, postcondition/3]).
%% Model wrappers
-export([start/1, echo/2, kill/1]).

-define(TEST_MODULE, gen_echoes).
-define(SUPERVISOR_MODULE, gen_echoes_sup).
-define(SUPERVISOR, echo_sup).
-define(SERVER_PREFIX, echo).

%% Initialize the state
initial_state() ->
    [stopped].

%% Command generator, S is the state
command(S) ->
    frequency([{40, {call, ?MODULE, start, [number_of_servers()]}},
               {40, {call, ?MODULE, echo, [oneof(S), message()]}},
               {20, {call, ?MODULE, kill, [oneof(S)]}}]).

message() ->
    string().

number_of_servers() ->
    ?SUCHTHAT(N, integer(), N > 0).

%% Next state transformation, S is the current state
next_state([stopped], _V, {call, _, start, [N]}) ->
    [ list_to_atom(atom_to_list(?SERVER_PREFIX) ++ integer_to_list(I)) || I <- lists:seq(1, N)];
next_state(S, _V, {call, _, _, _}) -> S.

%% Precondition, checked before command is added to the command sequence
precondition([stopped], {call, _, start, _}) -> true;
precondition(_, {call, _, start, _}) -> false;
precondition(_, {call, _, _, _}) -> true.

%% Postcondition, checked after command has been evaluated
%% OBS: S is the state before next_state(S,_,<command>)
postcondition([stopped], {call, _, start, [_N]}, Res) -> Res == ok;
postcondition([stopped], {call, _, echo, [_Who, _M]}, Res) ->
    Res == server_not_started;
postcondition([stopped], {call, _, kill, _}, Res) ->
    Res == server_not_started;
postcondition(_S, {call, _, echo, [Who, M]}, Res) ->
    Res == M ++ " from " ++ atom_to_list(Who);
postcondition(_S, {call, _, kill, _}, Res)   -> Res == ok;
postcondition(_S, {call, _, _, _}, _Res) -> false.

%% TEST PROPERTY
prop_echoserver() ->
    ?FORALL(Cmds, commands(?MODULE),
            begin
                startup(),
                {History, State, Res} = run_commands(?MODULE, Cmds),
                cleanup(State),
                ?WHENFAIL(io:format("H: ~p~nS: ~p~n Res: ~p~n",
                                    [History, State, Res]),
                          Res==ok)
            end).

%% Test function wrappers
start(N) ->
    {ok, _Pid} = ?SUPERVISOR_MODULE:start(N),
    ok.

echo(stopped, _Message) ->
    server_not_started;
echo(Server, Message) ->
    ?TEST_MODULE:echo(Server, Message).

kill(stopped) ->
    server_not_started;
kill(Server) ->
    Pid = whereis(Server),
    catch exit(Pid, kill),
    timer:sleep(10),
    ok.

%% Private utilitary stuff
startup() ->
    process_flag(trap_exit, true), % so that exit exceptions do not kill the tests
     % so no supervisor reports are printed out
    logger:set_primary_config(filters, [{no_sasl,
                                         {fun logger_filters:domain/2,
                                          {stop, equal, [otp, sasl]}}}]),
    ok.

cleanup(_Args) ->
    case whereis(?SUPERVISOR) of
        undefined -> ok;
        Pid ->
            true = exit(Pid, shutdown),
            timer:sleep(25),
            ok
    end.
