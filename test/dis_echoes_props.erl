%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A multi-instance supervisable echo server
%%%      (distributed version): test module
%%% @end
%%%-------------------------------------------------------------------
-module(dis_echoes_props).

-ifdef(testeqc).
-include_lib("eqc/include/eqc.hrl").
-include_lib("eqc/include/eqc_statem.hrl").
%-include_lib("proper/include/proper.hrl"). % do we hit a bug in proper (!?)

-include("dis_echoes.hrl").

%% Test property
-export([prop_echoserver/0]).
%% QuickCheck/PropER callbacks
-export([initial_state/0, command/1, precondition/2, next_state/3, postcondition/3]).
%% Model wrappers
-export([start/2, echo/2, kill/1]).

-define(TEST_MODULE, dis_echoes).
-define(SUPERVISOR_MODULE, dis_echoes_sup).
-define(SUPERVISOR, echo_sup).
-define(SERVER_PREFIX, echo).

%% Initialize the state
initial_state() ->
    [{alice, [stopped]},
     {bob,   [stopped]},
     {carol, [stopped]},
     {dan,   [stopped]}].

%% Command generator, S is the state
command(S) ->
    frequency([{40, {call, ?MODULE, start, [oneof(S), number_of_servers()]}},
               {40, {call, ?MODULE, echo, [oneof(S), message()]}},
               {20, {call, ?MODULE, kill, [oneof(S)]}}]).

message() ->
    list(char()). % string().

number_of_servers() ->
    ?SUCHTHAT(N, int(), N > 0).

%% Next state transformation, S is the current state
next_state(S, _V, {call, _, start, [{Server, [stopped]}, N]}) ->
    lists:keyreplace(Server, 1, S,
                     {Server, [list_to_atom(atom_to_list(?SERVER_PREFIX) ++
                                                integer_to_list(I))
                               || I <- lists:seq(1, N)]});
next_state(S, _V, {call, _, _, _}) -> S.

%% Precondition, checked before command is added to the command sequence
precondition(_, {call, _, _, _}) -> true.

%% Postcondition, checked after command has been evaluated
%% OBS: S is the state before next_state(S,_,<command>)
postcondition(_S, {call, _, start, [{_Server, [stopped]}, _N]}, Res) -> Res == ok;
postcondition(_S, {call, _, start, _}, Res) -> Res == already_started;
postcondition(_S, {call, _, echo, [{_Server, [stopped]}, _M]}, Res) -> Res == server_not_started;
postcondition(_S, {call, _, echo, [{Server, _Processes}, M]}, {Who, Res}) ->
    Res == M ++ " from " ++ atom_to_list(Who) ++
        " (running at " ++ atom_to_list(Server) ++ ?DOMAIN ++ ")";
postcondition(_S, {call, _, kill, [{_Server, [stopped]}]}, Res) -> Res == server_not_started;
postcondition(_S, {call, _, kill, [{_Server, _Processes}]}, Res) -> Res == ok;
postcondition(_S, {call, _, kill, _}, Res)   -> Res == ok;
postcondition(_S,  {call, _, _, _}, _Res) -> false.

%% TEST PROPERTY
prop_echoserver() ->
    ?FORALL(Cmds, commands(?MODULE),
            begin
                startup(),
                {History, State, Res} = run_commands(?MODULE, Cmds),
                cleanup(State),
                ?WHENFAIL(io:format("H: ~p~nS: ~p~n Res: ~p~n",
                                    [History, State, Res]),
                          Res==ok)
            end).

%% Test function wrappers
start({Node, [stopped]}, N) ->
    NodeName = list_to_atom(atom_to_list(Node) ++ ?DOMAIN),
    _Pid = rpc:block_call(NodeName, ?SUPERVISOR_MODULE, start, [N]),
    ok;
start({_Node, _Processes}, _N) ->
    already_started.

echo({_Server, [stopped]}, _Message) ->
    server_not_started;
echo({Server, Processes}, Message) ->
    % {ok, N} = proper_gen:pick(proper_types:oneof(Processes)),
    N = eqc_gen:pick(eqc_gen:oneof(Processes)),
    NodeName = list_to_atom(atom_to_list(Server) ++ ?DOMAIN),
    {N, ?TEST_MODULE:echo({N, NodeName}, Message)}.

kill({_Server,  [stopped]}) ->
    server_not_started;
kill({Server, Processes}) ->
    % {ok, N} = proper_gen:pick(proper_types:oneof(Processes)),
    N = eqc_gen:pick(eqc_gen:oneof(Processes)),
    NodeName = list_to_atom(atom_to_list(Server) ++ ?DOMAIN),
    Pid = rpc:call(NodeName, erlang, whereis, [N]),
    catch exit(Pid, kill),
    timer:sleep(10),
    ok.

%% Private utilitary stuff
startup() ->
    process_flag(trap_exit, true), % so that exit exceptions do not kill the tests
    % so no supervisor reports are printed out
    logger:set_primary_config(filters,
                              [{no_sasl,
                                {fun logger_filters:domain/2,
                                 {stop, equal, [otp, sasl]}}}]),
    [ {ok, _Node} = slave:start_link(Host,
                                     Name,
                                     " -setcookie " ++ ?TEST_COOKIE ++
                                     " -pa " ++ ?PATH) || {Name, Host} <- ?NODES ],
    ok.

cleanup(State) ->
    lists:foreach(fun({_Server, [stopped]}) -> ok;
                     ({Server, _Processes}) ->
                          NodeName = list_to_atom(atom_to_list(Server) ++ ?DOMAIN),
                          case rpc:call(NodeName, erlang, whereis, [?SUPERVISOR]) of
                              undefined -> ok;
                              Pid ->
                                  true = rpc:block_call(NodeName, erlang, exit, [Pid, shutdown]),
                                  timer:sleep(15),
                                  ok
                          end
                  end, State),
    [ ok = slave:stop(list_to_atom(atom_to_list(Name) ++ ?DOMAIN)) || {Name, _} <- ?NODES ].

-endif.
