%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A supervisable echo server (traditional test module)
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echo_tests).

-include_lib("eunit/include/eunit.hrl").

-define(ECHO_SERVER, echo).

gen_echo_terminate_normal_test() ->
    process_flag(trap_exit, true),
    {ok, Echo} = gen_echo:start(),
    true = exit(Echo, shutdown),
    timer:sleep(10),
    undefined = whereis(?ECHO_SERVER).

gen_echo_test_() ->
    {setup,
     fun() -> {ok, _Echo} = gen_echo:start() end,
     fun(_) -> ok = gen_server:stop(?ECHO_SERVER) end,
     fun(_) ->
             Message = "Lambda World Cádiz",
             {inparallel,
              [?_assertEqual(ok, gen_server:cast(?ECHO_SERVER, Message)),
               ?_assertEqual(Message, ?ECHO_SERVER ! Message),
               ?_assertEqual({ok, state}, gen_echo:code_change(oldvsn, state, extra)),
               ?_assertEqual([status], gen_echo:format_status(normal, [status]))
              ]}
     end}.
