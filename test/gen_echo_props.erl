%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A supervised echo server (test module)
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echo_props).

-include_lib("proper/include/proper.hrl").

%% PropER callbacks
-export([initial_state/0, command/1, precondition/2, next_state/3, postcondition/3]).
%% Model wrappers
-export([echo/1, kill/0]).

-define(TEST_MODULE, gen_echo).
-define(SUPERVISOR_MODULE, gen_echo_sup).
-define(SUPERVISOR, echo_sup).
-define(SERVER_NAME, echo).

%% Initialize the state
initial_state() ->
    [].

%% Command generator, S is the state
command(_S) ->
    frequency([{80, {call, ?MODULE, echo, [message()]}},
               {20, {call, ?MODULE, kill, []}}]).

message() ->
    string().

%% Next state transformation, S is the current state
next_state(S, _V, {call, _, _, _}) -> S.

%% Precondition, checked before command is added to the command sequence
precondition(_S, {call, _, _, _}) -> true.

%% Postcondition, checked after command has been evaluated
%% OBS: S is the state before next_state(S,_,<command>)
postcondition(_S, {call, _, echo, [M]}, Res) -> Res == M;
postcondition(_S, {call, _, kill, _}, Res)   -> Res == ok;
postcondition(_S, {call, _, _, _}, _Res) -> false.

%% TEST PROPERTY
prop_echoserver() ->
    ?FORALL(Cmds, commands(?MODULE),
            begin
                startup(),
                {History, State, Res} = run_commands(?MODULE, Cmds),
                cleanup(State),
                ?WHENFAIL(io:format("H: ~p~nS: ~p~n Res: ~p~n",
                                    [History, State, Res]),
                          Res==ok)
            end).

%% Test function wrappers
echo(Message) ->
    case catch ?TEST_MODULE:echo(Message) of
        Message -> Message;
        {'EXIT', {noproc, _}} -> server_not_started
    end.

kill() ->
    case whereis(?SERVER_NAME) of
        undefined -> already_stopped;
        Pid ->
            catch exit(Pid, kill),
            timer:sleep(10),
            ok
    end.

%% Private utilitary stuff
startup() ->
    process_flag(trap_exit, true), % so that exit exceptions do not kill the tests
    % so no supervisor reports are printed out
    logger:set_primary_config(filters,
                              [{no_sasl,
                                {fun logger_filters:domain/2,
                                 {stop, equal, [otp, sasl]}}}]),
    {ok, _Pid} = ?SUPERVISOR_MODULE:start(),
    ok.

cleanup(_Args) ->
    case whereis(?SUPERVISOR) of
        undefined -> ok;
        Pid ->
            true = exit(Pid, shutdown),
            timer:sleep(10),
            ok
    end.
