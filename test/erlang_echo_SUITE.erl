-module(erlang_echo_SUITE).

-include_lib("common_test/include/ct.hrl").

%% Test server callbacks
-export([all/0]).
%% Test cases
-export([mod_exists/1]).

% A Common Test suite file contains:
% * Init/end configuration function for the test suite
% * Init/end configuration function for a test case
% * Init/end configuration function for a test case group
% * Test cases

 all() ->
     [mod_exists].

 mod_exists(_) ->
    case code:is_loaded(echo) of
        {file, _} -> true;
        false -> {module, echo} = code:load_file(echo)
    end.